import request from "@/utils/request.js";
import { useTokenStore } from "@/stores/token.js";
// import { id } from "element-plus/es/locale";

// 文章分类列表查询
export const articleCategoryListService = () => {
  // const tokenStore = useTokenStore();
  // 在pinia中定义的响应式数据 都不需要.value
  // tokenStore.token 可以直接使用,不需要 .value
  // return request.get("/category", {
  //   headers: { Authorization: tokenStore.token },
  // });
  return request.get("/category");
};

// 添加文章分类接口
export const articleCategoryAddService = (categoryData) => {
  return request.post("/category", categoryData);
};

// 编辑文章分类接口
export const articleCategoryUpdateService = (categoryData) => {
  return request.put("/category", categoryData);
};

// 文章分类删除
export const articleCategoryDeleteService = (id) => {
  return request.delete("/category?id=" + id);
};

// 文章列表查询
export const articleListService = (params) => {
  return request.get("/article", { params: params });
};

// 添加文章函数
export const articleAddService = (articleData) => {
  return request.post("/article", articleData);
};

// 根据id获取文章详情
export const articleDetailService = (id) => {
  return request.get("/article/detail?id=" + id);
};

// 修改文章函数
export const articleUpdateService = (articleData) => {
  return request.put("/article", articleData);
};

// 删除文章函数
export const articleDeleteService = (id) => {
  return request.delete("/article?id=" + id);
};
