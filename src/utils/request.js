//定制请求的实例

//导入axios  npm install axios
import axios from "axios";
import { ElMessage } from "element-plus";
import { useTokenStore } from "@/stores/token";
//定义一个变量,记录公共的前缀  ,  baseURL
// const baseURL = 'http://localhost:8080';
const baseURL = "/api";
const instance = axios.create({ baseURL });

// 添加请求拦截器
// 使用axios拦截器，在发送请求前和请求错误时执行特定的函数
instance.interceptors.request.use(
    (config) => {
        /*
         * 请求前的回调函数
         * 主要用于对请求配置的预处理，例如在这里可以动态添加请求头中的token
         * @param {Object} config - 请求的配置对象
         * @returns {Object} 返回经过处理的请求配置对象
         */
        // 获取token存储实例
        const tokenStore = useTokenStore();
        // 检查是否存在token，并将其添加到请求头中
        if (tokenStore.token) {
            config.headers.Authorization = tokenStore.token;
        }
        return config;
    },
    (err) => {
        /*
         * 请求错误的回调函数
         * 当请求发生错误时（如网络故障等），会调用此函数，并将错误传递给它
         * @param {Object} err - 错误对象
         * @returns {Promise} 返回一个拒绝的Promise，以向调用方传递错误
         */
        Promise.reject(err);
    }
);


import router from '@/router'
// import { useRouter } from "vue-router";
// const router = useRouter();
//添加响应拦截器
// 在instance.interceptors.response中定义响应拦截器
instance.interceptors.response.use(
    result => {
        // 成功响应处理
        // 判断业务状态码
        if (result.data.code === 0) {
            return result.data;
        }
        // 处理操作失败，显示错误信息
        ElMessage.error(result.data.message ? result.data.message : "服务异常");
        return Promise.reject(result.data);
    },
    err => {
        // 错误响应处理
        // 判断响应状态码，如果是401，则表示未登录，需提示并跳转到登录页面
        if (err.response.status === 401) {
            ElMessage.error("请登录");
            router.push("/login");
        } else {
            // 其他错误，显示通用错误信息
            ElMessage.error("服务异常");
        }
        return Promise.reject(err); // 将异步错误状态转化为失败的状态
    }
);


export default instance;
