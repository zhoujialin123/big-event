// 定义store
import { defineStore } from "pinia";
import { ref } from "vue";

// 第一个参数是 名字 需要具有唯一性
// 第二个参数 函数名 函数的内部可以定义状态的所有内容
/**
 * 定义一个名为`token`的持久化存储模块
 *
 // * @returns 返回一个对象，包含：
 // *  - token: 响应式变量，用于存储token字符串
 *  - setToken: 函数，用于设置token的值
 *  - removeToken: 函数，用于移除token的值
 */
export const useTokenStore = defineStore(
    "token",
    () => {
        // 初始化token为一个空字符串的响应式变量
        const token = ref('');

        // 设置token值的函数
        const setToken = (newToken) => {
            token.value = newToken;
        };

        // 移除token值的函数，将其重置为空字符串
        const removeToken = () => {
            token.value = '';
        };

        // 返回包含token、setToken和removeToken的对象
        return { token, setToken, removeToken };
    },
    { persist: true } // 设置存储为持久化，确保token在刷新页面后仍然存在
);

